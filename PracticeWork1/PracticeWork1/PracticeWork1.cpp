﻿#include "pch.h"

using namespace std;

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @tparam Args Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @param args Переменые для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename... Args>
void input_check(const T& message, Args &... args)
{
	static_assert(std::is_same<T, char>::value || std::is_same<T, std::string>::value, "For message, use string types!");

	std::cout << message;

	while (!(std::cin >> ... >> args))
	{
		std::cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

		std::cout << "Error: " << message;
	}

	std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * Question to user [y/N] ...
 *
 * @return <tt>true</tt> - согласился, <tt>false</tt> - не согласился
 */
const bool answer_check()
{
	const char answer(std::cin.get()); // ввод ответа
	std::cin.unget();
	std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

	return (strchr("y|Y|д|Д|" /*ответы которые показывают согласие*/, answer) != NULL);
}

int main()
{
	Product example_product(34.99, 6, "Kivi"), // екземпляр класа для примера
		*product = new Product(); // рабочий екземпляр класса
	int count; // количество
	string name; // имя 
	float price; // цена

	cout << endl << fixed << setprecision(2) << "Example product \"" << example_product.get_name() << "\": " << endl
		<< "  Price:> " << example_product.get_price() << endl
		<< "  Count:> " << example_product.get_count() << endl
		<< "  Cost:> " << example_product.cost() << endl;

	do
	{
		cout << endl << "Enter product name:> ";
		getline(cin, name);
	} while (name.size() == 0);
	product->set_name(name);

	do
	{
		input_check([](const string &_name) -> string {
			return "Enter \"" + _name + "\" price:> ";
			}(product->get_name()), price);
	} while (price < 0);
	product->set_price(price);

	do
	{
		input_check([](const string &_name) -> string {
			return "Enter \"" + _name + "\" count:> ";
			}(product->get_name()), count);
	} while (count < 0);
	product->set_count(count);

	cout << endl << fixed << setprecision(2) << "Your product \"" << product->get_name() << "\": " << endl
		<< "  Price:> " << product->get_price() << endl
		<< "  Count:> " << product->get_count() << endl
		<< "  Cost:> " << product->cost() << endl;

	cout << endl << "Repeat [y/N]: ";

	return (answer_check() == false ? EXIT_SUCCESS : main());
}
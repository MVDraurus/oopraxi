#include "pch.h"
#include "Product.h"


Product::Product()
{
	_price = 0.0f;
	_count = 0;
}

Product::Product(const float _price, const unsigned int _count, const std::string _name)
{
	assert((_price >= 0 || _count >= 0 || _name.size() > 0) && "Error initialization object!");

	this->_price = _price;
	this->_count = _count;
	this->_name = _name;
}


Product::~Product()
{
}


// return the count 
const unsigned int Product::get_count() const noexcept
{
	return _count;
}


// Return the price
const float Product::get_price() const noexcept
{
	return _price;
}


// Set price value
void Product::set_price(const float& _price)
{
	assert(_price >= 0 && "Price not be less to zero!");

	this->_price = _price;
}


// Set count value.
void Product::set_count(const unsigned int& _count)
{
	assert(_count >= 0 && "Count not be less to zero!");

	this->_count = _count;
}


// Return the cost this product 
const float Product::cost() const noexcept
{
	return (_price <= 0 || _count <= 0 ? 0 : _price * _count);
}


// Set name value.
void Product::set_name(const std::string& _name)
{
	assert(_name.size() > 0 && "Size not be equal of zero!");

	this->_name = _name;
}


// Set name value.
void Product::set_name(const char* _name)
{
	assert(strlen(_name) > 0 && "Size not be equal of zero!");

	this->_name = std::string(_name);
}


// Get name value.
const std::string Product::get_name() const noexcept
{
	return _name;
}
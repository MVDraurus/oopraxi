#pragma once

#include <assert.h>

class Product
{
public:
	Product();
	Product(const float _price, const unsigned int _count, const std::string _name);
	~Product();

private:
	float _price;
	unsigned int _count;
	std::string _name;

public:
	// return the count
	const unsigned int get_count() const noexcept;
	// Return the price
	const float get_price() const noexcept;
	// Get name value.
	const std::string get_name() const noexcept;
	// Set price value
	void set_price(const float& _price);
	// Set count value.
	void set_count(const unsigned int& _count);
	// Return the cost this product
	const float cost() const noexcept;
	// Set name value.
	void set_name(const std::string& _name);
	// Set name value.
	void set_name(const char* _name);
};


#include "pch.h"


Array::Array(const size_t& _size) : _size(_size)
{
	assert(_size > 0 && "Size array not be, lass or equel zerro!");

	_array = new int[_size];

}


Array::~Array()
{
	delete[] _array;
}


// Set array value to instex.
void Array::set(const size_t& idx, int value)
{
	assert(idx >= 0 && idx < _size && "Index is not included in the array!");

	_array[idx] = value;
}


void Array::print() const
{
	std::cout << "Array: ";
	std::ostream_iterator<int> out_it(std::cout, ", ");
	std::copy(&_array[0], &_array[_size], out_it);
	std::cout << "\b\b." << std::endl;
}

// ������������ ������� ������.
const int Array::max()  const
{
	return *std::max_element(&_array[0], &_array[_size]);
}

// ���� �������� ������, ������������ �� ���������� ����������� ��������.
const int Array::sum(int sum)  const
{
	for (register int idx(_size); idx >= 0; idx--)
		if (_array[idx] > 0)
		{
			sum = 0;
			for (register size_t i(0); i < idx; i++)
				sum += _array[i];

			break;
		}

	return sum;
}

int& Array::get(const size_t& idx)
{
	assert(idx >= 0 && idx < _size && "Index is not included in the array!");

	return _array[idx];
}

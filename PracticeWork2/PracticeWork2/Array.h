#pragma once

#include "Array.h"
#include <assert.h>
#include <iterator>     // std::ostream_iterator
#include <algorithm>    // std::copy
#include <numeric>      // std::accumulate


class Array
{
public:
	Array(const size_t& _size);
	~Array();

private:
	int *_array;
	const size_t _size;
public:
	// Set array value to instex.
	void set(const size_t& idx, int value);
	void print() const;
	// ������������ ������� ������.
	const int max()  const;
	// ���� �������� ������, ������������ �� ���������� ����������� ��������.
	const int sum(int sum)  const;
	int& get(const size_t& idx);
};


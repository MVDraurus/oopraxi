﻿#include "pch.h"

using namespace std;

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * Пример ввода длины массива:
 * input_check("\nEnter size of array /> ", size, { [](const int& size)->const bool& {return size <= 0; } });
 *
 * @tparam Arg Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @tparam function Ламбда выражение, повторить при условии.
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename Arg>
void input_check(const T& message, Arg& arg,
	const function<const bool&(const Arg& var)>& lambda = NULL)
{
	static_assert(std::is_same<T, char>::value ||
		std::is_array<T>::value ||
		std::is_same<T, std::string>::value,
		"For message, use string types!");

	do
	{
		std::cout << message;

		while (!(std::cin >> arg))
		{
			std::cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

			std::cout << "Error: " << message;
		}

	} while (lambda != NULL && lambda(arg) == true);

	std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * Question to user [y/N] ...
 *
 * @return <tt>true</tt> - согласился, <tt>false</tt> - не согласился
 */
const bool answer_check()
{
	const char answer(std::cin.get()); // ввод ответа
	std::cin.unget();
	std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

	return (strchr("y|Y|д|Д|" /*ответы которые показывают согласие*/, answer) != NULL);
}

int main()
{
	Matrix example_matrix(6, 6), // Example matrix
		*matrix;
	int index, col, row;

	for (size_t i = 0; i < 6; i++)
		for (size_t j = 0; j < 6; j++)
			example_matrix.set(i, j, rand() % 10);

	system("cls");
	cout << "Your example_matrix[" << 6 << "][" << 6 << "]:> " << endl;
	example_matrix.print();
	index = example_matrix.run();
	cout << "Number one of the columns containing at least one zero element:> " <<
		(index == -1 ? "There is no column that contained at least one zero element!" : to_string(index + 1)) << endl << endl;

	// User matix
	input_check("Enter count rows of matrix /> ", row, { [](const int& var)->const bool& {return var <= 0; } });
	input_check("Enter count columns of matrix /> ", col, { [](const int& var)->const bool& {return var <= 0; } });

	matrix = new Matrix(row, col);

	cout << endl << "Fill matrix[" << row << "][" << col << "]:> " << endl;
	for (size_t i = 0; i < row; i++)
		for (size_t j = 0; j < col; j++)
			input_check([&]()->string {
			return "Enter matrix element[" + to_string(i) + "][" + to_string(j) + "]/> ";
				}(), matrix->get(i, j));

	cout << "Your matrix[" << row << "][" << col << "]:> " << endl;
	matrix->print();
	index = matrix->run();
	cout << "Number one of the columns containing at least one zero element:> " <<
		(index == -1 ? "There is no column that contained at least one zero element!" : to_string(index + 1)) << endl << endl;

	cout << endl << "Repeat [y/N]: ";
	return (answer_check() == true ? main() : EXIT_SUCCESS);
}
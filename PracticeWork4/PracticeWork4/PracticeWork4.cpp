﻿#include "pch.h"

using namespace std;

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * @tparam Args Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @param args Переменые для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename... Args>
void input_check(const T& message, Args &... args)
{
	static_assert(std::is_same<T, char>::value || std::is_same<T, std::string>::value, "For message, use string types!");

	std::cout << message;

	while (!(std::cin >> ... >> args))
	{
		std::cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

		std::cout << "Error: " << message;
	}

	std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * Question to user [y/N] ...
 *
 * @return <tt>true</tt> - согласился, <tt>false</tt> - не согласился
 */
const bool answer_check()
{
	const char answer(std::cin.get()); // ввод ответа
	std::cin.unget();
	std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

	return (strchr("y|Y|д|Д|" /*ответы которые показывают согласие*/, answer) != NULL);
}

std::ostream& operator << (std::ostream& out, const Complex& com)
{
	out << com.re << (com.im < 0 ? "+i(" : "+i") << com.im << (com.im < 0 ? ")" : "");
	return out;
}

std::istream& operator >> (std::istream& in, Complex& com)
{
	std::cout << "Введите действительную часть комплексного числа/> ";
	in >> com.re;
	std::cout << "Введите мнимую часть комплексного числа/> ";
	in >> com.im;

	return in;
}

int main()
{
	setlocale(0, "RUS");
	Complex com1, com2;

	cin >> com1 >> com2;

	std::cout << boolalpha
		<< "com1(" << com1 << ") >  com2(" << com2 << ") -> " << (com1 > com2) << endl
		<< "com1(" << com1 << ") <  com2(" << com2 << ") -> " << (com1 < com2) << endl
		<< "com1(" << com1 << ") == com2(" << com2 << ") -> " << (com1 == com2) << endl
		<< "com1(" << com1 << ") != com2(" << com2 << ") -> " << (com1 != com2) << endl
		<< "com1(" << com1 << ") +  com2(" << com2 << ") -> " << (com1 + com2) << endl
		<< "com1(" << com1 << ") -  com2(" << com2 << ") -> " << (com1 - com2) << endl
		<< "com1(" << com1 << ") *  com2(" << com2 << ") -> " << (com1 * com2) << endl
		<< "com1(" << com1 << ") /  com2(" << com2 << ") -> " << (com1 / com2) << endl
		<< "com1(" << com1 << ") += com2(" << com2 << ") -> " << (com1 += com2) << endl
		<< "com1(" << com1 << ") -= com2(" << com2 << ") -> " << (com1 -= com2) << endl
		<< "com1(" << com1 << ") *= com2(" << com2 << ") -> " << (com1 *= com2) << endl
		<< "com1(" << com1 << ") /= com2(" << com2 << ") -> " << (com1 /= com2) << endl
		<< "com1(" << com1 << ") =  com2(" << com2 << ") -> " << (com1 = com2) << endl;

	return EXIT_SUCCESS;
}